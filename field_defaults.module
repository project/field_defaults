<?php

/**
 * @file
 * Allows updating existing content with default values.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\field_defaults\Decorated\PreserveChangedItem;

/**
 * Implements hook_field_info_alter().
 */
function field_defaults_field_info_alter(array &$info): void {
  // @todo change config from retain to preserve.
  $info['changed']['class'] = PreserveChangedItem::class;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function field_defaults_form_field_config_edit_form_alter(&$form, FormStateInterface $form_state) {
  if (\Drupal::currentUser()->hasPermission('administer field defaults') && isset($form['default_value'])) {

    /** @var \Drupal\field\FieldConfigInterface $field */
    $field = $form_state->getFormObject()->getEntity();
    $field_language = $field->language();

    $form['default_value']['field_defaults'] = [
      '#type' => 'details',
      '#title' => t('Update existing content'),
      '#weight' => 100,
    ];
    $form['default_value']['field_defaults']['update_defaults'] = [
      '#title' => t('Overwrite existing <em>@lang</em> content with the selected default value(s)', ['@lang' => $field_language->getName()]),
      '#type' => 'checkbox',
    ];
    // Check that both field and bundle are translateable.
    $bundle_is_translatable = FALSE;
    if (\Drupal::hasService("content_translation.manager")) {
      $bundle_is_translatable = \Drupal::service('content_translation.manager')
        ->isEnabled($field->getTargetEntityTypeId(), $field->getTargetBundle());
    }

    if ($bundle_is_translatable && $field->isTranslatable()) {
      $field_language_id = $field_language->getId();
      $languages = \Drupal::languageManager()->getLanguages();

      $options = [];
      foreach ($languages as $language) {
        if ($language->getId() != $field_language_id) {
          $options[$language->getId()] = $language->getName();
        }
      }
      $form['default_value']['field_defaults']['update_defaults_lang'] = [
        '#type' => 'checkboxes',
        '#title' => t('Additionally Update entities of the following languages:'),
        '#options' => $options,
      ];
    }

    $form['default_value']['field_defaults']['no_overwrite'] = [
      '#type' => 'checkbox',
      '#title' => t('Keep existing values'),
      '#description' => t('This option will prevent overwriting fields with existing values.'),
    ];

    $form['actions']['submit']['#submit'][] = '_field_defaults_ui_submit';
  }
}

/**
 * Submit handler for field ui form.
 */
function _field_defaults_ui_submit(&$form, FormStateInterface $form_state) {
  $fieldDefaults = $form_state->getValue([
    'default_value_input',
    'field_defaults',
  ], FALSE);

  // At least one selection.
  $languages = array_filter($fieldDefaults['update_defaults_lang'] ?? []);
  if (!empty($fieldDefaults['update_defaults']) || !empty($languages)) {
    $fieldConfig = $form_state->getFormObject()->getEntity();
    // Only go ahead if field actually has value, 0 and FALSE are valid.
    $fieldValues = $form_state->getValue(['default_value_input', $fieldConfig->getName()]);
    if ($fieldValues !== NULL) {
      \Drupal::service('field_defaults.processor')->processFieldForm($fieldConfig, $fieldDefaults, $fieldValues);
    }
  }
}

/**
 * Implements hook_help().
 */
function field_defaults_help($path, $arg) {
  switch ($path) {
    case 'help.page.field_defaults':
      $output = '';
      $output .= '<h3>' . t('About Field Defaults') . '</h3>';
      $output .= '<p>' . t('This module works one field at a time.') . '</p>';
      $output .= '<p>' . t('It provides an additional checkbox option in the Default Value') . '</p>';
      $output .= '<p>' . t('section in the Manage Fields > Edit page for a given field.') . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<p>' . t('On save of the field edit screen, with this option checked, all entities') . '</p>';
      $output .= '<p>' . t('with this field attached will be updated with this fields default value,') . '</p>';
      $output .= '<p>' . t('regardless of whether another value already exists.') . '</p>';
      $output .= '<p>' . t('There is a settings form in Configuration -> System -> Field defaults settings') . '</p>';
      $output .= '<p>' . t('where you can enable/disable if you want to retain the original date.') . '</p>';
      return $output;
  }
}
